// Copyright Epic Games, Inc. All Rights Reserved.

#include "ArkanoidGameMode.h"
#include "ArkanoidPlayerController.h"
#include "ArkanoidCharacter.h"
#include "UObject/ConstructorHelpers.h"

AArkanoidGameMode::AArkanoidGameMode()
{
	// use our custom PlayerController class
	PlayerControllerClass = AArkanoidPlayerController::StaticClass();

	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/TopDownCPP/Blueprints/TopDownCharacter"));
	if (PlayerPawnBPClass.Class != nullptr)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}